package services

import (
	"fmt"
	"io"
	"log"
	"os"
)

const (
	Reset      = "\033[0m"
	Red        = "\033[31m"
	Green      = "\033[32m"
	Yellow     = "\033[33m"
	Cyan       = "\033[36m"
	BrightBlue = "\033[94m"
)

const (
	Error = 1 << iota
	Warn
	Info
	Debug
	Trace
	None = 0

	Default = Error | Warn
)

const (
	FLAGS  = log.Ldate | log.Ltime | log.Lmicroseconds
	PREFIX = "\t[%s%s%s]\t"
)

type Logger struct {
	loggingLevel  int
	consoleOutput bool

	logger *log.Logger
}

func NewLogger(loggingLevel int, output io.Writer) *Logger {
	consoleOutput := output == os.Stderr || output == os.Stdout

	return &Logger{
		loggingLevel:  loggingLevel,
		consoleOutput: consoleOutput,

		logger: log.New(output, "", FLAGS),
	}
}

func (l *Logger) OK(args ...any) {
	if l.loggingLevel != None {
		if l.consoleOutput {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, Green, "OK", Reset))
		} else {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, "", "OK", ""))
		}
		l.logger.Println(args...)
	}
}

func (l *Logger) Error(args ...any) {
	if l.loggingLevel&Error != 0 {
		if l.consoleOutput {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, Red, "ERROR", Reset))
		} else {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, "", "ERROR", ""))
		}
		l.logger.Println(args...)
	}
}

func (l *Logger) Warn(args ...any) {
	if l.loggingLevel&Warn != 0 {
		if l.consoleOutput {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, Yellow, "WARN", Reset))
		} else {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, "", "WARN", ""))
		}
		l.logger.Println(args...)
	}
}

func (l *Logger) Info(args ...any) {
	if l.loggingLevel&Info != 0 {
		if l.consoleOutput {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, Cyan, "INFO", Reset))
		} else {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, "", "INFO", ""))
		}
		l.logger.Println(args...)
	}
}

func (l *Logger) Debug(args ...any) {
	if l.loggingLevel&Debug != 0 {
		if l.consoleOutput {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, BrightBlue, "DEBUG", Reset))
		} else {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, "", "DEBUG", ""))
		}
		l.logger.Println(args...)
	}
}

func (l *Logger) Trace(args ...any) {
	if l.loggingLevel&Trace != 0 {
		if l.consoleOutput {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, BrightBlue, "TRACE", Reset))
		} else {
			l.logger.SetPrefix(fmt.Sprintf(PREFIX, "", "TRACE", ""))
		}
		l.logger.Println(args...)
	}
}
